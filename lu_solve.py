import numpy as np

from copy import deepcopy
from functools import reduce
from utils import p, solve_lower_triangular_matrix, solve_upper_triangular_matrix

class LUSolve:
  def __init__(self, A, b, *args, **kwargs):
    self.m_arr = []
    self.p_arr = []
    self.P = None
    self.A = A
    self.b = b
    self.U = None
    self.L = None

  def convert_to_lu(self, matrix):
    for (i,j), value in np.ndenumerate(matrix):
      if i == j:
        if value == 0:
          # find item to swap rows
          under_item_values = matrix[i+1:,j]
          not_zero_under_item_values = np.where(under_item_values != 0)
          if (len(not_zero_under_item_values[0]) == 0):
            raise Exception("Can't decompose to LU decomposition")
          first_non_zero_under_item_values_index = not_zero_under_item_values[0][0]+j+1

          # swap rows
          matrix[[i, first_non_zero_under_item_values_index]] = matrix[[first_non_zero_under_item_values_index, i]]
          self.p_arr.append(p(np.identity(matrix.shape[0]),i, first_non_zero_under_item_values_index))
        else:
          self.p_arr.append(np.identity(matrix.shape[0])) # indentity matrix

        # now we ensured that the diag item is not zero
        matrix = self.make_zeros_under(matrix, i)

    # print('U = \n', matrix)
    self.U = matrix
    

    # calculate L
    p2 = self.p_arr[::-1]
    m2 = self.m_arr[::-1]
    self.P = reduce(np.matmul, self.p_arr)

    temp_l = []
    for i in range(len(self.m_arr)):
      temp_l.append(m2[i])
      temp_l.append(p2[i])

    temp_result = reduce(np.matmul, temp_l)

    b = np.linalg.inv(temp_result)
    self.L = np.matmul(self.P, b)
    
    return (self.L, self.U)

  def make_zeros_under(self, matrix, col_index):
    item_value = matrix[col_index, col_index]
    under_item_values = matrix[col_index+1:,col_index]

    m_func = lambda x: -x / item_value

    # i can store this array
    m_array = m_func(under_item_values)

    # store
    m_matrix = np.identity(matrix.shape[0])
    m_matrix[col_index+1:,col_index] = m_array
    self.m_arr.append(m_matrix)

    should_change_matrix = matrix[col_index+1:,:]
    
    for (i, j), value in np.ndenumerate(should_change_matrix):
      matrix[col_index+1+i,j] = value + m_array[i] * matrix[col_index,j]

    return matrix

  def solve(self):
    L, U = self.convert_to_lu(self.A)

    b = np.matmul(self.P, self.b)
    y = solve_lower_triangular_matrix(L, b)


    x = solve_upper_triangular_matrix(U, y)
    xn = np.linalg.solve(U, y)

    return x