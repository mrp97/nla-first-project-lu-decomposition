import typing
import numpy as np
from lu_solve import LUSolve

A = np.array([
  [0, 1, -1, 1],
  [1, 1, -1, 2],
  [-1, -1, 1, 0],
  [1, 2, 0, 2]
])

b = np.array([1,2,3,4])

problem = LUSolve(A, b)
x = problem.solve()

print('---------------')
print('x = ', x)
print('L = \n', problem.L)
print('U = \n', problem.U)
print('---------------')
for i, m in enumerate(problem.m_arr):
  print(f'M({i}) = \n', m)

print('---------------')
for i, p in enumerate(problem.p_arr):
  print(f'P({i}) = \n', p)
