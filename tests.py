import pytest
import numpy as np
from main import *
from functools import reduce

m_arr = []
p_arr = []

# برنامه‌ای در مطلب یا پایتون بنویسید که در آن ماتریس دلخواه وارون‌پذیر A و بردار سمت راست b را به عنوان ورودی بگیرد و پس از یافتن تجزیه مثلثی ماتریس(A=LU) (در صورت وجود)، دستگاه Ax=b را با استفاده از آن حل نماید. توجه کنید که در صورت عدم وجود تجزیه مثلثی، پیغام مناسبی دهید.

def test_jaygasht():
  matrix = np.identity(3)
  target = [[0,1,0],[1,0,0],[0,0,1]]
  assert (p(matrix, 0, 1) == target).all()


def test_pivot():
  matrix = np.array([
    [0, 1, -1, 1],
    [1, 1, -1, 2],
    [-1, -1, 1, 0],
    [1, 2, 0, 2]
  ])
  assert pivot(matrix) == []

def test_make_zeros_under():
  matrix = np.array([
    [ 1, 1, -1, 2],
    [ 0, 1, -1, 1],
    [-1,-1, 1, 0],
    [ 1, 2, 0, 2]
  ])
  # desired matrix is not correct
  desired_matrix = np.array(
    [
      [ 1, 1, -1, 2],
      [ 0, 1, -1, 1],
      [ 0, 0, 0, 2],
      [ 0, 1, 1, 0]
    ]
  )

  assert (make_zeros_under(matrix, 0) == desired_matrix).all() 


def test_solve_upper_triangular_matrix():
  R = [
    [2, 7, 1, 8, 2],
    [0, 8, 1, 8, 2],
    [0, 0, 8, 4, 5],
    [0, 0, 0, 9, 0],
    [0, 0, 0, 0, 4],
  ]
  b = [3, 1, 4, 1, 5]
  
  a = solve_upper_triangular_matrix(R, b)

  assert None == []