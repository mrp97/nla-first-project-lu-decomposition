
from copy import deepcopy
from fractions import Fraction as frac

def p(matrix,x,y):
  a = deepcopy(matrix)
  a[[x, y]] = a[[y, x]]

  return a


def solve_upper_triangular_matrix(R, b):
  to_return = deepcopy(b)

  # Convert R and b to frac
  for x, line in enumerate(R):
    for y, el in enumerate(line):
      R[x][y] = frac(el)
  for x, el in enumerate(b):
    to_return[x] = frac(el)

  # The solution will be here
  for step in range(len(b) - 1, 0 - 1, -1):
    if R[step][step] == 0:
      if to_return[step] != 0:
        return "No solution"
      else:
        return "Infinity solutions"
    else:
      to_return[step] = to_return[step] / R[step][step]

    for row in range(step - 1, 0 - 1, -1):
      to_return[row] -= R[row][step] * to_return[step]
  
  return to_return


def solve_lower_triangular_matrix(L, b):
  to_return = deepcopy(b)

  for x, line in enumerate(L):
    for y, el in enumerate(line):
      L[x][y] = frac(el)
  for x, el in enumerate(b):
    to_return[x] = frac(el)

  for step in range(0, len(b)):
    if L[step][step] == 0:
      if to_return[step] != 0:
        return "No solution"
      else:
        return "Infinity solutions"
    else:
      to_return[step] = to_return[step] / L[step][step]

    for row in range(step + 1, len(b)):
      to_return[row] -= L[row][step] * to_return[step]

  return to_return
